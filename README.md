# 学习CocoaPods

[TOC]

## 1. 概述

此片文章主要介绍Cocoapods的一些基本命令、Podfile文件、Podspec文件及创建私有的Pods库。如果需要了解CocoaPods的原理及更多内容请查看[官网](https://guides.cocoapods.org)。

## 2. 本文讲述内容

### 1. Command API介绍

**pod init**

    这个命令就会再工程根目录下面生成一个Podfile文件,podfile用文本编辑就行了

**pod install**

    这个是第一次在工程里面使用pods的时候使用，并且，也是每次你编辑你的Podfile（添加、移除、更新）的时候使用。

**pod update**

    当你运行 pod update PODNAME 命令时，CocoaPods会帮你更新到这个库的新版本，而不需要考虑Podfile.lock里面的限制，它会更新到这个库尽可能的新版本，只要符合Podfile里面的版本限制。

**pod install & pod update**

        pod install 在podfile 添加、移除、更新都会去处理Podfile.lock文件同时更新Pods库，如果Podfile.lock存在锁定的库的版本且符合Podfile文件的限制，执行pod install就只会下载锁定的版本  
        pod update 库名 如果这个库有新的版本，并且新版本仍然符合在Podfile里的限制，它就会被更新。  
        pod update 不指定库名的话，CocoaPods就会更新每一个Podfile里面的库到尽可能的最新版本且要符合Podfile文件里边的限制。

**pod outdated**

    当执行 pod outdated 的时候，CocoaPods 会在已安装的 Pod 中（出现在 Podfile.lock 中）找出所有有新版本可用的 Pod，这些 pod 在满足 Podfile 中要求的情况下，可以被更新

```
yang:XXTGuangDong yangguoqiang$ pod outdated
Updating spec repo `master`

CocoaPods 1.2.1.beta.1 is available.
To update use: `sudo gem install cocoapods --pre`
[!] This is a test version we'd love you to try.

For more information, see https://blog.cocoapods.org and the CHANGELOG for this version at https://github.com/CocoaPods/CocoaPods/releases/tag/1.2.1.beta.1

Analyzing dependencies
The following pod updates are available:
- ActionSheetPicker-3.0 1.6.2 -> 1.6.2 (latest version 2.2.0)
- Activity 0.1.0 -> 0.1.0 (latest version 1.0)
- AFNetworking 2.6.3 -> 2.6.3 (latest version 3.1.0)
- APAddressBook 0.1.14 -> 0.1.14 (latest version 0.3.1)
- Base 0.1.0 -> 0.1.0 (latest version 0.8.0)
- Bugly 1.4.8 -> 1.4.8 (latest version 2.4.7)
- JLRoutes 1.5.5 -> 1.5.5 (latest version 2.0.2)
- JSONModel 1.0.2 -> 1.0.2 (latest version 1.7.0)
- JSPatch 0.2 -> 0.2 (latest version 1.1.3)
- Mantle 1.5.8 -> 1.5.8 (latest version 2.1.0)
- MBProgressHUD 0.9.2 -> 0.9.2 (latest version 1.0.0)
- Reachability 3.1.1 -> 3.1.1 (latest version 3.2)
- ReactiveCocoa 2.5 -> 2.5 (latest version 5.0.1)
- SDWebImage 3.7.6 -> 3.7.6 (latest version 4.0.0)
- SKUtils 0.1.0 -> 0.1.0 (latest version 1.0.0)
- SQLCipher 3.1.0 -> 3.1.0 (latest version 3.4.1)
- UMengSocial 4.2.5 -> 4.2.5 (latest version 5.0)
yang:XXTGuangDong yangguoqiang$ 

```

**pod deintegrate**

    执行完上面这一步之后，cocoapods实际上已经和你的项目已经解除关系了。但是，cocoapods的文件还是存在的，这些需要你自己手动去删除了。实际上你不删除也不影响你的项目

**pod env**

    展示一些CocoaPods的环境配置信息
Stack
```
   CocoaPods : 1.2.0
        Ruby : ruby 2.3.3p222 (2016-11-21 revision 56859) [x86_64-darwin15]
    RubyGems : 2.5.2
        Host : Mac OS X 10.11.6 (15G31)
       Xcode : 8.2.1 (8C1002)
         Git : git version 2.10.1 (Apple Git-78)
Ruby lib dir : /usr/local/Cellar/ruby/2.3.3/lib
Repositories : bitbucket-ygqspecs - git@bitbucket.org:yangguoqiang/ygqspecs.git @ e9fcb687c7355bd4795233fda2c67ffc03db6052
               master - https://github.com/CocoaPods/Specs.git @ d06b1f74ec529f434090b979e29063d3bd2712ad
               YgqSpecs - https://yangguoqiang@bitbucket.org/yangguoqiang/ygqspecs.git @ e9fcb687c7355bd4795233fda2c67ffc03db6052
```
Installation Source
```
Executable Path: /usr/local/bin/pod
```
Plugins
```
cocoapods-deintegrate : 1.0.1
cocoapods-plugins     : 1.0.0
cocoapods-search      : 1.0.0
cocoapods-stats       : 1.0.0
cocoapods-trunk       : 1.1.2
cocoapods-try         : 1.1.0
```

**pod search**

    pod search xxx 来查找第三方库

**pod list**

    列出所有可使用的pods库

**pod try**

    pod try xxx执行命令后，会下载库的范例，并用xcode打开范例

举例

```
yang:~ yangguoqiang$ pod try AFNetworking --no-repo-update

Trying AFNetworking
1: Example/AFNetworking Example.xcodeproj
2: Example/AFNetworking tvOS Example.xcodeproj
3: AFNetworking.xcworkspace
Which project would you like to open
1
Opening '/private/var/folders/q5/75c5kyld4910rjpt_jsfwc0r0000gn/T/CocoaPods/Try/AFNetworking/Example/AFNetworking Example.xcodeproj'
yang:~ yangguoqiang$ 
```

**pod spec create**

    会在一个已有项目中，创建一个podspec文件。

```
yang:testCocoaPods yangguoqiang$ ls
TestCocoaPods
yang:testCocoaPods yangguoqiang$ cd TestCocoaPods/
yang:TestCocoaPods yangguoqiang$ ls
Build			Podfile			TestCocoaPods.xcodeproj
DerivedData		TestCocoaPods
yang:TestCocoaPods yangguoqiang$ pod spec create TestCocoaPods

Specification created at TestCocoaPods.podspec
yang:TestCocoaPods yangguoqiang$ 
```

**pod lib create**

    pod lib create XXX命令执行后会创建一个以XXX为名CocoaPods模版。

```
yang:test yangguoqiang$ pod lib create testCocoaPods
Cloning `https://github.com/CocoaPods/pod-template.git` into `testCocoaPods`.
Configuring testCocoaPods template.
security: SecKeychainSearchCopyNext: The specified item could not be found in the keychain.

------------------------------

To get you started we need to ask a few questions, this should only take a minute.

If this is your first time we recommend running through with the guide: 
 - http://guides.cocoapods.org/making/using-pod-lib-create.html
 ( hold cmd and double click links to open in a browser. )


What language do you want to use?? [ Swift / ObjC ]
 > ObjC

Would you like to include a demo application with your library? [ Yes / No ]
 > Yes

Which testing frameworks will you use? [ Specta / Kiwi / None ]
 > None 

Possible answers are [ Specta / Kiwi / None ]
 > None

Would you like to do view based testing? [ Yes / No ]
 > Yes

What is your class prefix?
 > YGQ
security: SecKeychainSearchCopyNext: The specified item could not be found in the keychain.
security: SecKeychainSearchCopyNext: The specified item could not be found in the keychain.
security: SecKeychainSearchCopyNext: The specified item could not be found in the keychain.
security: SecKeychainSearchCopyNext: The specified item could not be found in the keychain.
security: SecKeychainSearchCopyNext: The specified item could not be found in the keychain.
security: SecKeychainSearchCopyNext: The specified item could not be found in the keychain.

Running pod install on your new library.

Analyzing dependencies
Fetching podspec for `testCocoaPods` from `../`
Downloading dependencies
Installing FBSnapshotTestCase (2.1.4)
Installing testCocoaPods (0.1.0)
Generating Pods project
Integrating client project

[!] Please close any current Xcode sessions and use `testCocoaPods.xcworkspace` for this project from now on.
Sending stats
Pod installation complete! There are 2 dependencies from the Podfile and 2 total pods installed.

 Ace! you're ready to go!
 We will start you off by opening your project in Xcode
  open 'testCocoaPods/Example/testCocoaPods.xcworkspace'

To learn more about the template see `https://github.com/CocoaPods/pod-template.git`.
To learn more about creating a new pod, see `http://guides.cocoapods.org/making/making-a-cocoapod`.
yang:test yangguoqiang$ 
```

**pod spec lint**

    本地或者远端验证podspec文件是否正确。

**pod lib lint**

    本地验证podspec文件是否正确。

**pod spec cat**

    显示指点库名的podspec文件的内容

```
yang:~ yangguoqiang$ pod spec cat AFNetworking
{
  "name": "AFNetworking",
  "version": "3.1.0",
  "license": "MIT",
  "summary": "A delightful iOS and OS X networking framework.",
  "homepage": "https://github.com/AFNetworking/AFNetworking",
  "social_media_url": "https://twitter.com/AFNetworking",
  "authors": {
    "Mattt Thompson": "m@mattt.me"
  },
  "source": {
    "git": "https://github.com/AFNetworking/AFNetworking.git",
    "tag": "3.1.0",
    "submodules": true
  },
  "requires_arc": true,
  "public_header_files": "AFNetworking/AFNetworking.h",
  "source_files": "AFNetworking/AFNetworking.h",
  "prefix_header_contents": "#ifndef TARGET_OS_IOS\n  #define TARGET_OS_IOS TARGET_OS_IPHONE\n#endif\n\n#ifndef TARGET_OS_WATCH\n  #define TARGET_OS_WATCH 0\n#endif\n\n#ifndef TARGET_OS_TV\n  #define TARGET_OS_TV 0\n#endif",
  "platforms": {
    "ios": "7.0",
    "osx": "10.9",
    "watchos": "2.0",
    "tvos": "9.0"
  },
  "subspecs": [
    {
      "name": "Serialization",
      "source_files": "AFNetworking/AFURL{Request,Response}Serialization.{h,m}",
      "public_header_files": "AFNetworking/AFURL{Request,Response}Serialization.h",
      "watchos": {
        "frameworks": [
          "MobileCoreServices",
          "CoreGraphics"
        ]
      },
      "ios": {
        "frameworks": [
          "MobileCoreServices",
          "CoreGraphics"
        ]
      },
      "osx": {
        "frameworks": "CoreServices"
      }
    },
    {
      "name": "Security",
      "source_files": "AFNetworking/AFSecurityPolicy.{h,m}",
      "public_header_files": "AFNetworking/AFSecurityPolicy.h",
      "frameworks": "Security"
    },
    {
      "name": "Reachability",
      "platforms": {
        "ios": "7.0",
        "osx": "10.9",
        "tvos": "9.0"
      },
      "source_files": "AFNetworking/AFNetworkReachabilityManager.{h,m}",
      "public_header_files": "AFNetworking/AFNetworkReachabilityManager.h",
      "frameworks": "SystemConfiguration"
    },
    {
      "name": "NSURLSession",
      "dependencies": {
        "AFNetworking/Serialization": [

        ],
        "AFNetworking/Security": [

        ]
      },
      "ios": {
        "dependencies": {
          "AFNetworking/Reachability": [

          ]
        }
      },
      "osx": {
        "dependencies": {
          "AFNetworking/Reachability": [

          ]
        }
      },
      "tvos": {
        "dependencies": {
          "AFNetworking/Reachability": [

          ]
        }
      },
      "source_files": "AFNetworking/AF{URL,HTTP}SessionManager.{h,m}",
      "public_header_files": "AFNetworking/AF{URL,HTTP}SessionManager.h"
    },
    {
      "name": "UIKit",
      "platforms": {
        "ios": "7.0",
        "tvos": "9.0"
      },
      "dependencies": {
        "AFNetworking/NSURLSession": [

        ]
      },
      "public_header_files": "UIKit+AFNetworking/*.h",
      "source_files": "UIKit+AFNetworking"
    }
  ]
}
yang:~ yangguoqiang$ 
```

**pod spec which**

    显示本地cocoapod映像的pods库的路径。

```
pod spec which AFNetworking
/Users/yangguoqiang/.cocoapods/repos/master/Specs/a/7/5/AFNetworking/3.1.0/AFNetworking.podspec.json
yang:~ yangguoqiang$ 
```

**pod spec edit**

    打开podspec文件进行编辑

** pod trunk register**

    注册CocoaPods

```
yang:~ yangguoqiang$ pod trunk register ygqym101620@163.com 'ygqym101620'
[!] Please verify the session by clicking the link in the verification email that has been sent to ygqym101620@163.com
yang:~ yangguoqiang$ 
```

**pod trunk me**

    查看自己的账号信息，以及账号拥有的仓库。

```
yang:~ yangguoqiang$ pod trunk me
  - Name:     ygqym101620
  - Email:    ygqym101620@163.com
  - Since:    March 17th, 03:10
  - Pods:     None
  - Sessions:
    - March 17th, 03:10 -       Unverified. IP: 113.140.31.190
    - March 17th, 03:15 - July 23rd, 03:19. IP: 113.140.31.190
yang:~ yangguoqiang$ 
```

**pod trunk info**

    显示pods库的信息。

```
yang:~ yangguoqiang$ pod trunk info SDWebImage

SDWebImage
    - Versions:
      - 2.4 (2014-05-19 21:38:21 UTC)
      - 2.5 (2014-05-19 21:39:04 UTC)
      - 2.6 (2014-05-19 21:41:09 UTC)
      - 2.7 (2014-05-19 21:44:13 UTC)
      - 2.7.4 (2014-05-19 21:48:44 UTC)
      - 3.0 (2014-05-19 21:45:15 UTC)
      - 3.1 (2014-05-19 21:46:01 UTC)
      - 3.2 (2014-05-19 21:48:48 UTC)
      - 3.3 (2014-05-19 21:52:34 UTC)
      - 3.4 (2014-05-19 21:54:25 UTC)
      - 3.5 (2014-05-19 21:56:42 UTC)
      - 3.5.1 (2014-05-19 21:59:21 UTC)
      - 3.5.2 (2014-05-19 22:00:46 UTC)
      - 3.5.3 (2014-05-19 22:04:17 UTC)
      - 3.5.4 (2014-05-19 22:04:17 UTC)
      - 3.6 (2014-05-19 22:05:17 UTC)
      - 3.7.0 (2014-07-15 06:56:43 UTC)
      - 3.7.1 (2014-07-23 05:22:28 UTC)
      - 3.7.2 (2015-03-17 15:52:03 UTC)
      - 3.7.3 (2015-07-13 20:19:49 UTC)
      - 3.7.4 (2016-01-08 15:46:45 UTC)
      - 3.7.5 (2016-01-21 14:50:01 UTC)
      - 3.7.6 (2016-05-08 17:45:31 UTC)
      - 3.8.0 (2016-06-06 18:28:30 UTC)
      - 3.8.1 (2016-06-07 09:11:28 UTC)
      - 3.8.2 (2016-09-05 17:20:38 UTC)
      - 4.0.0 (2017-01-28 18:24:23 UTC)
      - 4.0.0-beta (2016-10-05 09:31:19 UTC)
      - 4.0.0-beta2 (2016-10-06 14:28:34 UTC)
    - Owners:
      - Bogdan Poplauschi <bpoplauschi@gmail.com>
      - Olivier Poitrey <rs@dailymotion.com>
yang:~ yangguoqiang$ 
```
**pod trunk push**

    pod trunk push XXX.podspec 把自己podspec文件上传到trunk服务器(https://github.com/CocoaPods/Specs)

**pod trunk add-owner**

    pod trunk add-owner podName 529037336@qq.com 给自己的podName库增加一个拥有者，此增加的拥有者必须是cocoapods的注册者。成功后，这个拥有者便可以对这个pod库进行操作，比如更新版本等。

**pod trunk remove-owner**

    pod trunk remove-owner podName 529037336@qq.com 给自己的podName库移除拥有者。

**pod trunk deprecate**

    pod trunk deprecate podName 让这个podName库过期

**pod trunk delete**

    pod trunk delete podName version 删除podName库的某个版本，此操作不可回退。

**pod repo add**

    pod repo add repoName git地址 从git服务器克隆一份podspec库到本地cocoapod／repos

```
yang:~ yangguoqiang$ cd ~/.cocoapods/
yang:.cocoapods yangguoqiang$ ls
repos
yang:.cocoapods yangguoqiang$ cd repos/
yang:repos yangguoqiang$ ls
YgqSpecs		bitbucket-ygqspecs	master
```

**pod repo update**

    pod repo update repoName 更新以repoName为名的库

```
yang:repos yangguoqiang$ pod repo update YgqSpecs
Updating spec repo `YgqSpecs`
  $ /usr/bin/git -C /Users/yangguoqiang/.cocoapods/repos/YgqSpecs fetch origin
  $ /usr/bin/git -C /Users/yangguoqiang/.cocoapods/repos/YgqSpecs rev-parse
  --abbrev-ref HEAD
  master
  $ /usr/bin/git -C /Users/yangguoqiang/.cocoapods/repos/YgqSpecs reset --hard
  origin/master
  HEAD is now at b29fbc3 [Add] PhotoAlbumHelpTool (1.0.4)
yang:repos yangguoqiang$ 
```

**pod reop list**

    显示本地所有的cocoapods库的信息

```
yang:~ yangguoqiang$ pod repo list

bitbucket-ygqspecs
- Type: git (master)
- URL:  git@bitbucket.org:yangguoqiang/ygqspecs.git
- Path: /Users/yangguoqiang/.cocoapods/repos/bitbucket-ygqspecs

master
- Type: git (master)
- URL:  https://github.com/CocoaPods/Specs.git
- Path: /Users/yangguoqiang/.cocoapods/repos/master

YgqSpecs
- Type: git (master)
- URL:  https://yangguoqiang@bitbucket.org/yangguoqiang/ygqspecs.git
- Path: /Users/yangguoqiang/.cocoapods/repos/YgqSpecs

3 repos
yang:~ yangguoqiang$ 
```

**pod repo remove**

    pod repo remove podName 移除本地podName私有库

**pod repo push**

    pod repo push podName name.podspec 将name.podspec推送到podName私有库里

**pod setup**

    pod setup 克隆公有的Specs库到本地，确保本地是最新的

**pod ipc list**

    pod ipc list 打印所有本地库的作者，摘要，描述及支持的平台。

```
yang:~ yangguoqiang$ pod ipc list
---
EaseMobSDKFull:
  authors:
  - ygqym101620
  summary: An Objective-C client for IM and Real-time voice service.
  description: An Objective-C client for IM and Real-time voice service ,yang qiang
    qiang pod spec
  platforms:
  - ios
HelpTool:
  authors:
  - ygqym101620
  summary: private HelpTool.
  description: Provide tools class methods
  platforms:
  - ios
ScrollviewCarousel:
  authors:
  - ygqym101620
  summary: ScrollviewCarousel.
  description: By the custom images
  platforms:
  - ios
UMengMessage:
  authors:
  - ygqym101620
  summary: UMengMessage RemoteNotification
  description: A short description of UMengMessage test
  platforms:
  - ios
```

**pod ipc spec**

    将podspec文件转成json格式

```
yang:HelpTool yangguoqiang$ pod ipc spec HelpTool.podspec 
{
  "name": "HelpTool",
  "version": "1.0.2",
  "summary": "private HelpTool.",
  "description": "Provide tools class methods",
  "homepage": "https://yangguoqiang@bitbucket.org/yangguoqiang/helptool.git",
  "license": "MIT",
  "authors": {
    "ygqym101620": "yanggq101620@163.com"
  },
  "source": {
    "git": "https://yangguoqiang@bitbucket.org/yangguoqiang/helptool.git",
    "tag": "1.0.2"
  },
  "platforms": {
    "ios": "8.0"
  },
  "requires_arc": true,
  "source_files": "HelpTool/Classes/**/*.{h,m}",
  "frameworks": "UIKit",
  "dependencies": {
    "ReactiveCocoa": [

    ],
    "MBProgressHUD": [

    ]
  }
}
```

**pod plugins list**

    所有已知插件列表

```
yang:~ yangguoqiang$ pod plugins list
Downloading Plugins list...

Available CocoaPods Plugins:

-> AppleDoc
     Generates docset and documentation for a pod.
     - Gem:     cocoapods-appledoc
     - URL:     https://github.com/CocoaPods/cocoapods-appledoc

-> Deploy
     Deploys dependencies for a CocoaPods project without needing to clone the
     repo (Similar to Bundler's `--deployment`).
     - Gem:     cocoapods-deploy
     - URL:     https://github.com/jcampbell05/cocoapods-deploy

-> Rome
     Rome makes it easy to build a list of frameworks for consumption outside of
     Xcode, e.g. for a Swift script.
     - Gem:     cocoapods-rome
     - URL:     https://github.com/neonichu/rome

-> Deintegrate (1.0.1)
     Deintegrates a project from CocoaPods.
     - Gem:     cocoapods-deintegrate
     - URL:     https://github.com/kylef/cocoapods-deintegrate

-> Pod Dependencies
     Shows a project's CocoaPod dependency graph.
     - Gem:     cocoapods-dependencies
     - URL:     https://github.com/segiddins/cocoapods-dependencies

-> Pod browser
     Open a pod's homepage in the browser.
     - Gem:     cocoapods-browser
     - URL:     https://github.com/dealforest/cocoapods-browser

-> Check Latest
     Checks if the latest version of a pod is up to date.
     - Gem:     cocoapods-check_latest
     - URL:     https://github.com/yujinakayama/cocoapods-check_latest

-> Pod docs
     Convenient access to the documentation of a Pod via cocoadocs.org.
     - Gem:     cocoapods-docs
     - URL:     https://github.com/CocoaPods/cocoapods-docs

-> docstats
     Showing documentation metrics of Pods.
     - Gem:     cocoapods-docstats
     - URL:     https://github.com/neonichu/cocoapods-docstats

-> open
     Open a pod’s workspace.
     - Gem:     cocoapods-open
     - URL:     https://github.com/leshill/open_pod_bay

-> Pod info
     Shows information on installed Pods.
     - Gem:     cocoapods-podfile_info
     - URL:     https://github.com/cocoapods/cocoapods-podfile_info

-> repo-svn
     Adds subversion support to manage spec-repositories.
     - Gem:     cocoapods-repo-svn
     - URL:     https://github.com/clarkda/cocoapods-repo-svn

-> repo-hg
     Adds mercurial support to manage spec-repositories.
     - Gem:     cocoapods-repo-hg
     - URL:     https://github.com/clarkda/cocoapods-repo-hg

-> Pod try (1.1.0)
     Quickly try the demo project of a Pod.
     - Gem:     cocoapods-try
     - URL:     https://github.com/CocoaPods/cocoapods-try

-> Pod watch
     Watch for Podfile changes and run pod install.
     - Gem:     cocoapods-watch
     - URL:     https://github.com/supermarin/cocoapods-watch

-> Pods Roulette
     Builds an empty project with three random pods.
     - Gem:     cocoapods-roulette
     - URL:     https://github.com/sirlantis/cocoapods-roulette

-> Sorted Search
     Adds a sort subcommand for pod search to sort search results by amount of
     stars, forks, or github activity.
     - Gem:     cocoapods-sorted-search
     - URL:     https://github.com/DenHeadless/cocoapods-sorted-search

-> Release
     Tags and releases pods for you.
     - Gem:     cocoapods-release
     - URL:     https://github.com/Sparrow-Labs/cocoapods-release

-> cocoapods clean
     Remove Podfile.lock, Pods/ and *.xcworkspace.
     - Gem:     cocoapods-clean
     - URL:     https://github.com/BendingSpoons/cocoapods-clean

-> CocoaPods Keys
     Store sensitive data in your Mac's keychain, that will be installed into
     your app's source code via the Pods library.
     - Gem:     cocoapods-keys
     - URL:     https://github.com/orta/cocoapods-keys

-> CocoaPods Packager
     Generate a framework or static library from a podspec.
     - Gem:     cocoapods-packager
     - URL:     https://github.com/CocoaPods/cocoapods-packager

-> CocoaPods Links
     A CocoaPods plugin to manage local development pods
     - Gem:     cocoapods-links
     - URL:     https://github.com/mowens/cocoapods-links

-> CocoaPods Prune Localizations
     Upon running pod install, this plugin will remove unused localizations by
     your project
     - Gem:     cocoapods-prune-localizations
     - URL:     https://github.com/dtorres/cocoapods-prune-localizations

-> CocoaPods Readonly
     Developers switching from submodules are used to modifying library source
     files from within Xcode. This locks those files as needed so Xcode warns
     you when attempting to edit them.
     - Gem:     cocoapods-readonly
     - URL:     https://github.com/Yelp/cocoapods-readonly

-> CocoaPods Thumbs
     Use cocoapods-thumbs to check upvotes or downvotes of Podspecs from your
     peers based on past experiences.
     - Gem:     cocoapods-thumbs
     - URL:     https://github.com/quadion/cocoapods-thumbs

-> CocoaPods Blacklist
     Check if a project is using a banned version of a pod. Handy for security
     audits.
     - Gem:     cocoapods-blacklist
     - URL:     https://github.com/yahoo/cocoapods-blacklist

-> CocoaPods Superdeintegrate
     Deletes the CocoaPods cache, your derived data folder, and makes sure that
     your Pods directory is gone.
     - Gem:     cocoapods-superdeintegrate
     - URL:     https://github.com/ashfurrow/cocoapods-superdeintegrate

-> CocoaPods Archive
     cocoapods-archive plugin that archive your project
     - Gem:     cocoapods-archive
     - URL:     https://github.com/fjbelchi/cocoapods-archive

-> CocoaPods Check
     Displays differences between locked and installed Pods
     - Gem:     cocoapods-check
     - URL:     https://github.com/square/cocoapods-check

-> CocoaPods Acknowledgements
     CocoaPods plugin that generates an acknowledgements plist to make it easy
     to create tools to use in apps.
     - Gem:     cocoapods-acknowledgements
     - URL:     https://github.com/CocoaPods/cocoapods-acknowledgements

-> CocoaPods Generator
     Add files to empty target from *.podspec, such as souce files, libraries,
     frameworks, resources and so on.
     - Gem:     cocoapods-generator
     - URL:     https://github.com/zhzhy/cocoapods-generator

-> CocoaPods Debug
     A simple plugin to ease debugging CocoaPods.
     - Gem:     cocoapods-debug
     - URL:     https://github.com/segiddins/cocoapods-debug

-> CocoaPods Artifactory Plugin
     Enables usage of Artifactory as an index for CocoaPods repo and as a
     repository for pods.
     - Gem:     cocoapods-art
     - URL:     https://github.com/JFrogDev/cocoapods-art

-> CocoaPods Update If You Dare
     update ... if you dare (warns users before updating all pods at once)
     - Gem:     cocoapods-update-if-you-dare
     - URL:     https://github.com/Ashton-W/cocoapods-update-if-you-dare

-> CocoaPods Uploader
     Upload file/dir to remote storage.
     - Gem:     cocoapods-uploader
     - URL:     https://github.com/alibaba/cocoapods-uploader

-> CocoaPods Mix Frameworks
     Mix use_framework! targets with static targets through a surrogate
     Framework target.
     - Gem:     cocoapods-mix-frameworks
     - URL:     https://github.com/flovilmart/cocoapods-mix-frameworks
yang:~ yangguoqiang$ 
```

**pod plugins search**

    pod plugins search xxx 根据名字搜索插件

```
yang:~ yangguoqiang$ pod plugins search Uploader
Downloading Plugins list...

Available CocoaPods Plugins matching 'Uploader':

-> CocoaPods Uploader
   Upload file/dir to remote storage.
   - Gem:     cocoapods-uploader
   - URL:     https://github.com/alibaba/cocoapods-uploader
yang:~ yangguoqiang$ 
```

**pod plugins installed**

    显示所有安装的插件及版本

```
yang:~ yangguoqiang$ pod plugins installed

Installed CocoaPods Plugins:
    - cocoapods-deintegrate : 1.0.1
    - cocoapods-plugins     : 1.0.0
    - cocoapods-search      : 1.0.0
    - cocoapods-stats       : 1.0.0 (post_install hook)
    - cocoapods-trunk       : 1.1.2
    - cocoapods-try         : 1.1.0
yang:~ yangguoqiang$ 
```

**pod chache list**

    pod cache list name 输出以name为名的库的缓存记录。

```
yang:~ yangguoqiang$ pod cache list SDWebImage
SDWebImage:
  - Version: 3.7.6
    Type:    Release
    Spec:    /Users/yangguoqiang/Library/Caches/CocoaPods/Pods/Specs/Release/SDWebImage/3.7.podspec.json
    Pod:     /Users/yangguoqiang/Library/Caches/CocoaPods/Pods/Release/SDWebImage/3.7.6-c325c
  - Version: 4.0.0
    Type:    Release
    Spec:    /Users/yangguoqiang/Library/Caches/CocoaPods/Pods/Specs/Release/SDWebImage/4.0.podspec.json
    Pod:     /Users/yangguoqiang/Library/Caches/CocoaPods/Pods/Release/SDWebImage/4.0.0-76a63
yang:~ yangguoqiang$ 
```

**pod cache clean**

    pod cache clean name 清除以name为名的库的缓存记录,可以选择清除的版本或者全部清除。

```
yang:~ yangguoqiang$ pod cache clean SDWebImage
1: SDWebImage v3.7.6 (Release)
2: SDWebImage v4.0.0 (Release)
Which pod cache do you want to remove?
```

### 2. Podfile语法介绍

    Podfile 是一个文件，用于定义项目所需要使用的第三方库

**(1)podfile举例**

```
source 'https://github.com/CocoaPods/Specs.git'
source 'https://github.com/Artsy/Specs.git'

platform :ios, '9.0'
inhibit_all_warnings!

target 'MyApp' do
  pod 'GoogleAnalytics', '~> 3.1'
  target 'MyAppTests' do
    inherit! :search_paths
    pod 'OCMock', '~> 2.0.1'
  end
end

post_install do |installer|
  installer.pods_project.targets.each do |target|
    puts target.name
  end
end
```

**(2)细节详细说明**
source 'https://github.com/CocoaPods/Specs.git'
source 'https://github.com/Artsy/Specs.git'

    指点的库的下载源地址 如果不添加默认是源是cocoapods的共有库 如果podfile文件中有私有依赖库必须指定私有库的specs文件地址

platform :ios , '9.0'

    指定平台为iOS，且支持的系统版本在9.0以上 其他平台如果：OS X tvOS等。

pod命令

    pod 'Libarary' 永远使用最新的的libarary版本
    pod 'Libarary','2.0' 使用固定的2.0版本 不会去更新
    pod 'Libarary', '~>1.1.0' 使用 [1.1.0,2.0.0)之间的版本。
    pod 'Libarary', '<2.0' 要求版本小于2.0,当超过2.0后版本不在更新
    pod 'Libarary', '<=2.0' 要版本小于或者等于2.0,超过2.0后不在更新
    > 和 >= 的不在赘述
    pod 'Libarary', :path =>'localPath' 本地libarary库的路径
    pod 'Libarary', :git  =>'remotePath'  远程库的地址（默认是master）
    pod 'Libarary', :git => 'remotePaht',:branch=>'dev' 分支 ／:tag=>'0.7.0'指定的tag ／:commit=>'082f8319af'指定提交的版本
    pod 'PonyDebugger', :configurations => ['Release', 'App Store'] 在Release和App Store模式引用该库
    pod 'PonyDebugger', :configuration => ['Release'] 在Release模式引用该库
    pod 'QueryKit/Attribute' 只引用QueryKit的Attribute子库，不包含其它子库
    pod 'QueryKit', :subspecs => ['Attribute', 'QuerySet']  只引用QueryKit的Attribute和QuerySet子库，不包含其它子库
    

inhibit_all_warnings!
    
    屏蔽cocoapods库里边所有的警告。可以全局屏蔽，也可以单独屏蔽某一个库的警告。
    例如:pod 'SSZipArchive', :inhibit_warnings =>true

use_frameworks!
    
    通过use_frameworks!来编译cocoaTouch Framework的动态框架。因为现在的Swift只能被编译为动态框架，所以如果代码中依赖包含有Swift代码，又想使用CocoaPods来管理
    的话，必须开启这个选项。
    use_frameworks！会把项目的依赖全部改为framework。

Carthage介绍

    Carthage是在Cocoa Touch Framework 和Swift发布后出现的专门针对Framework进行的包管理工具。

Carthage和CocoaPods比较

    Carthage只支持动态框架，它仅负责将项目clone到本地并将对应的Cocoa Framework target进行构建，之后你需要自己将构建好的framework添加到项目中。
    而Cocoapods只要配置好，无需手动添加。
    
    # Cartfile
    github "ReactiveCocoa/ReactiveCocoa"
    github "onevcat/Kingfisher" ~> 1.8
    github "https://enterprise.local/hello/repo.git"
    
    Carthage是直接从git仓库中获取项目,而不需要依靠像Cocoapods 里边的podspec文件。

target
```
    定义一个依赖库与target（Xcode project）的关系。每个target应该对应一个Xcode target。默认情况下，子target的依赖关系是包含父target的，除非指定非继承父target。
    
    target 'ShowsApp' do
    pod 'ShowsKit'

  # Has its own copy of ShowsKit + ShowTVAuth
  target 'ShowsTV' do
    pod 'ShowTVAuth'
  end

  # Has its own copy of Specta + Expecta
  # and has access to ShowsKit via the app
  # that the test target is bundled into

  target 'ShowsTests' do
    inherit! :search_paths
    pod 'Specta'
    pod 'Expecta'
  end
end
```

```
# Note: There are no targets called "Shows" in any of this workspace's Xcode projects
abstract_target 'Shows' do
  pod 'ShowsKit'

  # The target ShowsiOS has its own copy of ShowsKit (inherited) + ShowWebAuth (added here)
  target 'ShowsiOS' do
    pod 'ShowWebAuth'
  end

  # The target ShowsTV has its own copy of ShowsKit (inherited) + ShowTVAuth (added here)
  target 'ShowsTV' do
    pod 'ShowTVAuth'
  end

  # Our tests target has its own copy of
  # our testing frameworks, and has access
  # to ShowsKit as well because it is
  # a child of the abstract target 'Shows'

  target 'ShowsTests' do
    inherit! :search_paths
    pod 'Specta'
    pod 'Expecta'
  end
end
```
    
project

```
# This Target can be found in a Xcode project called `FastGPS`
target 'MyGPSApp' do
  project 'FastGPS'
  ...
end

# Same Podfile, multiple Xcodeprojects
target 'MyNotesApp' do
  project 'FastNotes'
  ...
end
```

def

```
我们还可以通过def命令来声明一个pod集：

def 'CustomPods'
   pod 'IQKeyboardManagerSwift'
end 
然后，我们就可以在需要引入的target处引入之：

target 'MyTarget' do 
   CustomPods
end 
这么写的好处是：如果有多个target，而不同target之间并不全包含，那么可以通过这种方式来分开引入。
```

pre_install

    当我们下载完成，但是还没有安装之时，会勾起来，然后可以通过pre_install指定要做的事，做完后才进入安装阶段。
    
```
pre_install do |installer|
  # Do something fancy!
end
```

post_install

    当我们安装完成，但是生成的工程还没有写入磁盘之时，我们可以指定要执行的操作。
    
```
post_install do |installer|
  installer.pods_project.targets.each do |target|
    target.build_configurations.each do |config|
      config.build_settings['GCC_ENABLE_OBJC_GC'] = 'supported'
    end
  end
end
```

### 3. Podspec语法介绍

podspec文件举例

```
Pod::Spec.new do |spec|
  spec.name         = 'Reachability'
  spec.version      = '3.1.0'
  spec.license      = { :type => 'BSD' }
  spec.homepage     = 'https://github.com/tonymillion/Reachability'
  spec.authors      = { 'Tony Million' => 'tonymillion@gmail.com' }
  spec.summary      = 'ARC and GCD Compatible Reachability Class for iOS and OS X.'
  spec.source       = { :git => 'https://github.com/tonymillion/Reachability.git', :tag => 'v3.1.0' }
  spec.module_name  = 'Rich'

  spec.ios.deployment_target  = '9.0'
  spec.osx.deployment_target  = '10.10'

  spec.source_files       = 'Reachability/common/*.swift'
  spec.ios.source_files   = 'Reachability/ios/*.swift', 'Reachability/extensions/*.swift'
  spec.osx.source_files   = 'Reachability/osx/*.swift'

  spec.framework      = 'SystemConfiguration'
  spec.ios.framework  = 'UIKit'
  spec.osx.framework  = 'AppKit'

  spec.dependency 'SomeOtherPod'
end
```

social_media_url

    spec.social_media_url = 'https://twitter.com/cocoapods'
    社交网址，如微博

homepage

    spec.homepage         = 'https://yangguoqiang@bitbucket.org/yangguoqiang/photoalbumhelptool.git'
    库的地址

source

    spec.source           = { :git => 'https://yangguoqiang@bitbucket.org/yangguoqiang/photoalbumhelptool.git', :tag => s.version.to_s }
    Git仓库地址，例如在Github地址后边加上 .git 就是Git仓库地址

documentation_url

    spec.documentation_url = 'http://www.example.com/docs.html'
    文档地址

dependency

    spec.dependency 'AFNetworking', '~> 1.0' 依赖共有库
    pod 'LPPushService', :git => 'https://github.com/xiaofei86/LPPushService.git', :tag => '1.0.0' 依赖自己的私有库

requires_arc

    spec.requires_arc = true 支持arc

frameworks

    spec.frameworks = 'QuartzCore', 'CoreData' 添加所依赖的系统库

librarys

    spec.libraries = 'xml2', 'z' 添加所依赖的系统静态库


vendored_frameworks

    spec.vendored_frameworks = 'MyFramework.framework', 'TheirFramework.framework' 添加所依赖的第三方库


vendored_librarys

    spec.vendored_libraries = 'libProj4.a', 'libJavaScriptCore.a' 添加所依赖的第三方静态库


prefix_header_file

     spec.prefix_header_file = 'iphone/include/prefix.pch' 给库田间一个以prefix.pch为名的头文件
   

prefix_header_contents

    spec.prefix_header_contents = '#import <UIKit/UIKit.h>', '#import <Foundation/Foundation.h>'
    给头文件添加引用

source_files

    spec.source_files = 'Classes/**/*.{h,m}' pod的资源文件


resource_bundle

    spec.ios.resource_bundle = { 'MapBox' => 'MapView/Map/Resources/*.png' } 在pod中打包，图片，音频文件等


resource

    spec.resource = 'Resources/HockeySDK.bundle'  在pod中打包，图片，音频文件等


resource 和 resource_boundle的区别

```
使用resource的pod所带来的资源和app target资源在同一个boundle层，很容易产生命名冲突。
使用resource_bundles 是在bundle层面上做了分组，不会导致命名冲突。

```

subspec

    对pod库进行了模块化分组，使用的时候 可以添加部分或全部模块


```
Pod::Spec.new do |s|
  s.name = 'RestKit'

  s.subspec 'Core' do |cs|
    cs.dependency 'RestKit/ObjectMapping'
    cs.dependency 'RestKit/Network'
    cs.dependency 'RestKit/CoreData'
  end

  s.subspec 'ObjectMapping' do |os|
  
  end
end
```


    

### 4. Creat Private Pods介绍

**实际操作**